import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:app01/models/card.dart';
import 'package:app01/components/colors.dart';
import 'package:app01/pages/add_card.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future<String> _getList() async {
    final response =
        await http.get(Uri.parse('https://all-lottery.com/result-lottery.php'));
    return response.body;
  }

  Widget showCardList() {
    return FutureBuilder(
        future: _getList(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          List<Widget> cardList;
          if (snapshot.hasData) {
            var cardData = jsonDecode(snapshot.data);
            List<CardId> lists = cardIdFromJson(jsonEncode(cardData['data']));
            cardList = <Widget>[
              Column(
                children: lists.map((cardList) {
                  return Container(
                      margin: EdgeInsets.all(8),
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 8,
                            offset: Offset(0, 2),
                          ),
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset('assets/images/card_preload.png',
                              width: 632),
                          Text(
                            'CARD NUMBER',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 12,
                              letterSpacing: 2,
                            ),
                          ),
                        ],
                      ));
                }).toList(),
              )
            ];
          } else {
            cardList = <Widget>[
              SizedBox(
                child: CircularProgressIndicator(),
                width: 60,
                height: 60,
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text("Loading"),
              )
            ];
          }
          return Center(
            child: Column(
              children: cardList,
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Kard Keeper'),
          leading:
              Image.asset('assets/images/logo.png', width: 40.0, height: 40.0),
        ),
        body: Container(
          padding: const EdgeInsets.only(bottom: 130.0), // Add bottom padding
          color: kTertiaryColor, // Set the background color here
          child: Center(
            child: ListView(
              children: <Widget>[showCardList()],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: Container(
          alignment: Alignment.bottomCenter,
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => AddCard()));
            },
            backgroundColor: Colors.white,
            foregroundColor: kPrimaryColor,
            elevation: 8.0,
            child: const Icon(Icons.add, size: 42.0),
          ),
        ));
  }
}
