import 'package:flutter/material.dart';

MaterialColor getMaterialColor(Color color) {
  final Map<int, Color> shades = {
    50: Color.fromRGBO(136, 14, 79, .1),
    100: Color.fromRGBO(136, 14, 79, .2),
    200: Color.fromRGBO(136, 14, 79, .3),
    300: Color.fromRGBO(136, 14, 79, .4),
    400: Color.fromRGBO(136, 14, 79, .5),
    500: Color.fromRGBO(136, 14, 79, .6),
    600: Color.fromRGBO(136, 14, 79, .7),
    700: Color.fromRGBO(136, 14, 79, .8),
    800: Color.fromRGBO(136, 14, 79, .9),
    900: Color.fromRGBO(136, 14, 79, 1),
  };
  return MaterialColor(color.value, shades);
}

const kPrimaryColor = Color(0xFF164AB0);
const kSecondaryColor = Color(0xFF3573FB);
const kTertiaryColor = Color(0xFF4D87F8);
const kQuaternaryColor = Color(0xFF2D3343);
