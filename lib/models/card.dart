// To parse this JSON data, do
//
//     final cardId = cardIdFromJson(jsonString);

import 'dart:convert';

List<CardId> cardIdFromJson(String str) =>
    List<CardId>.from(json.decode(str).map((x) => CardId.fromJson(x)));

String cardIdToJson(List<CardId> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CardId {
  CardId({
    required this.id,
    required this.idNumber,
    required this.name,
    required this.birthDate,
    required this.address,
  });

  int id;
  String idNumber;
  String name;
  String birthDate;
  String address;

  factory CardId.fromJson(Map<String, dynamic> json) => CardId(
        id: json["id"],
        idNumber: json["idNumber"],
        name: json["name"],
        birthDate: json["birthDate"],
        address: json["address"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "idNumber": idNumber,
        "name": name,
        "birthDate": birthDate,
        "address": address,
      };
}
